from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum, Max
from siop.settings import UPLOAD_MONITOREO_DIR


class Flota(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    embarcacion = models.CharField(max_length=100, null=True)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion


class Arte(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    lance = models.BooleanField()
    palangre = models.BooleanField()
    cong = models.BooleanField()
    fresq = models.BooleanField()
    flota = models.ForeignKey(Flota, null=True)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion

class Jurisdiccion(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion


class Puerto(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    corta = models.CharField(max_length=100, null=True)
    co_tip_cue = models.IntegerField()
    co_cue_agu = models.IntegerField()
    co_uni_pol = models.IntegerField(null=True)
    co_puerto = models.IntegerField(null=True)
    pna_puerto = models.IntegerField(null=True)
    jurliquida = models.ForeignKey(Jurisdiccion, related_name='jurLiquida', null=True)
    jurisdiccion = models.ForeignKey(Jurisdiccion, related_name='jurisdiccion', null=True)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion


class Rectangulo(models.Model):
    nro_rect = models.IntegerField()
    area = models.CharField(max_length=4)
    m200 = models.BooleanField(default=False)
    malvinas = models.BooleanField(default=False)
    in_out = models.BooleanField(default=False)
    m12 = models.BooleanField(default=False)
    pcia100 = models.BooleanField(default=False)
    brotola = models.BooleanField(default=False)
    raya = models.BooleanField(default=False)
    zcp = models.BooleanField(default=False)
    golfo = models.BooleanField(default=False)
    var_cfp = models.BooleanField(default=False)
    congrio_c = models.BooleanField(default=False)
    vieira = models.BooleanField(default=False)
    porc_rio = models.FloatField(null=True)
    porc_zcpo = models.FloatField(null=True)
    porc_prov = models.FloatField(null=True)
    cuarto = models.CharField(max_length=2, null=True)
    zona = models.CharField(max_length=1, null=True)
    ylat = models.FloatField()
    xlong = models.FloatField()
    rincon = models.CharField(max_length=2, null=True)
    creado = models.DateField()

    def __unicode__(self):
		return 'Area:'+self.area+' Rect:'+str(self.nro_rect)


class Fondo(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=100)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion


class Especie(models.Model):
    nro_espc = models.CharField(max_length=3)
    desc_espc = models.CharField(max_length=100)
    sinonimo_1 = models.CharField(max_length=100, null=True)
    nbre_cient = models.CharField(max_length=100, null=True)
    tipo = models.CharField(max_length=100)
    cuotas = models.BooleanField()
    variado = models.BooleanField()
    fao_cod = models.CharField(max_length=3, null=True)
    fao_esp = models.CharField(max_length=100, null=True)
    fao_nombre = models.CharField(max_length=100, null=True)
    pelagicas = models.BooleanField()
    g_pelagicos = models.BooleanField()
    rayas = models.BooleanField()
    tiburones = models.BooleanField()
    tipo_espc = models.IntegerField(null=True)
    # fauna es True si va columna en consulta de Fauna
    fauna = models.BooleanField(default=False)
    # padre es una Especie creada solo a efectos de agrupar otras especies
    # usada para agrupar kgs captura en el Fauna
    padre = models.ForeignKey('self', null=True)
    creado = models.DateField()

    def __unicode__(self):
        return self.desc_espc


class Armador(models.Model):
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=256)
    creado = models.DateField()

    def __unicode__(self):
        return self.descripcion

class Lote(models.Model):
    fecha_recibido = models.DateField()
    fecha_importado = models.DateTimeField(auto_now=True)
    # usuario = models.ForeignKey(User, related_name='usuario_lote_parte')
    archivo_fuente = models.FileField(upload_to='partes/fuentes')
    anio = models.IntegerField()

    def __unicode__(self):
		return str(self.fecha_importado)+'-id:'+str(self.id)

    @staticmethod
    def get_ult_fecha_recibida(anio):
        '''
        :param anio:
        :return: un date con la ultima fecha recibida del anio
        '''
        try:
            fecha=Lote.objects.filter(anio=anio).aggregate(Max('fecha_recibido'))
        except:
            fecha=None
        return fecha['fecha_recibido__max']


# Buques.dbf
class Buque(models.Model):
    sradial = models.CharField(max_length=100, null=True)
    matbuq = models.CharField(max_length=100)
    nombuq = models.CharField(max_length=100, null=True)
    armador = models.ForeignKey(Armador, null=True)
    senial_lw = models.CharField(max_length=100, null=True)
    nro_reg = models.CharField(max_length=100, null=True)
    flot_emb = models.ForeignKey(Flota, null=True)
    arte1 = models.ForeignKey(Arte, null=True, related_name='arte1')
    arte2 = models.ForeignKey(Arte, null=True, related_name='arte2')
    arte_flota = models.ForeignKey(Arte, null=True, related_name='arteFlota')
    eslora = models.FloatField(null=True)
    manga = models.FloatField(null=True)
    puntal = models.FloatField(null=True)
    trn = models.FloatField(null=True)
    trb = models.FloatField(null=True)
    bodega = models.FloatField(null=True)
    bod_neta = models.FloatField(null=True)
    bod_obs = models.TextField(null=True)
    cajones = models.FloatField(null=True)
    silo_hielo = models.FloatField(null=True)
    band_anter = models.CharField(max_length=100, null=True)
    mescon = models.CharField(max_length=100, null=True)
    aniocon = models.CharField(max_length=100, null=True)
    aniobotadur = models.CharField(max_length=100, null=True)
    capt_parej = models.BooleanField()
    resol_514 = models.BooleanField()
    coptobas = models.CharField(max_length=100, null=True)
    ptobas40_0 = models.CharField(max_length=100, null=True)
    ptobas_fec = models.DateField(null=True)
    copaicon = models.CharField(max_length=100, null=True)
    comatcas = models.CharField(max_length=100, null=True)
    omi = models.CharField(max_length=100, null=True)
    perm_web = models.BooleanField()
    frent_mari = models.BooleanField()
    user_modi = models.CharField(max_length=100, null=True)
    fcha_modi = models.DateField(null=True)
    coop = models.CharField(max_length=100, null=True)
    triptot = models.FloatField(null=True)
    ctarte1 = models.FloatField(null=True)
    ctarte2 = models.FloatField(null=True)
    arte3 = models.ForeignKey(Arte, null=True, related_name='arte3')
    ctarte3 = models.FloatField(null=True)
    opbaja = models.CharField(max_length=100, null=True)
    calmax = models.FloatField(null=True)
    velcru = models.FloatField(null=True)
    velarr = models.FloatField(null=True)
    cpagua = models.FloatField(null=True)
    cpcomb = models.FloatField(null=True)
    autonom = models.FloatField(null=True)
    tripdm = models.FloatField(null=True)
    astcon = models.CharField(max_length=100, null=True)
    norma_lega = models.TextField(null=True)
    observacio = models.TextField(null=True)
    cert_merl = models.BooleanField()
    mat_sat = models.CharField(max_length=100, null=True)
    provincial = models.BooleanField()
    mat3 = models.CharField(max_length=100, null=True)
    ftabinorma = models.CharField(max_length=100, null=True)
    lote = models.ForeignKey(Lote, null=True)
    anio = models.IntegerField()

    def __unicode__(self):
        return self.nombuq

# Prts.dbf
class Parte(models.Model):
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    nro_prt = models.CharField(max_length=100)
    nuevamat = models.CharField(max_length=100, null=True)
    buque = models.ForeignKey(Buque, null=True)
    arm_buque = models.CharField(max_length=100, null=True)
    tipo_flta = models.ForeignKey(Flota, null=True)
    pto_salida = models.ForeignKey(Puerto, null=True, related_name='ptoSalida')
    pto_desem = models.ForeignKey(Puerto, null=True, related_name='ptoDesem')
    fch_salida = models.DateTimeField()
    fch_desem = models.DateTimeField()
    combust = models.FloatField(null=True)
    millas = models.FloatField(null=True)
    tripulac = models.FloatField(null=True)
    cod_art1 = models.ForeignKey(Arte, null=True, related_name='arte1Parte')
    cod_art2 = models.ForeignKey(Arte, null=True, related_name='arte2Parte')
    capt_prt = models.FloatField(null=True)
    env = models.BooleanField()
    nro_viaje = models.IntegerField(null=True)
    env_infra = models.DateField(null=True)
    diario = models.BooleanField()
    env_conso = models.DateField(null=True)
    m12 = models.BooleanField()
    prorrateo = models.BooleanField()
    fch_app = models.DateField(null=True)
    rectific = models.BooleanField()
    mod_rectific = models.CharField(max_length=100, null=True)
    causamodi = models.CharField(max_length=100, null=True)
    pagoaranc = models.BooleanField()
    revisado = models.BooleanField()
    validado = models.BooleanField()
    cod_perm = models.IntegerField(null=True)
    parte_new = models.BooleanField()
    # hora_zarp= datetime de fch_salida
    # hora_desem= datetime de fch_desem
    nro_artes = models.IntegerField(null=True)
    error = models.BooleanField()
    parte_web = models.BooleanField()
    obs = models.TextField(null=True)
    anio = models.IntegerField()

    def __unicode__(self):
        return self.nro_prt + '-Jur:' + str(self.jurisdiccion)

    @staticmethod
    # total de viajes por buque en un anio dado
    def get_total_viajes(anio, buque):
        total_viajes = Parte.objects.filter(buque=buque).count()
        return total_viajes


# Cajones.dbf
class Cajon(models.Model):
    anio_ejerc = models.CharField(max_length=100, null=True)
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    especie = models.ForeignKey(Especie, null=True)
    cajones = models.FloatField(null=True)
    env = models.BooleanField()
    fch_app = models.DateField(null=True)
    anio = models.IntegerField()


# Rcts.dbf
class Rect_Parte(models.Model):
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    cuadricula = models.ForeignKey(Rectangulo, null=True)  # rectangulo+area
    rectangulo = models.CharField(max_length=100, null=True)
    area = models.CharField(max_length=100, null=True)
    lances = models.IntegerField(null=True)
    tiem_pesca = models.FloatField(null=True)
    fondo = models.ForeignKey(Fondo, null=True)
    profund = models.FloatField(null=True)
    estado_tie = models.CharField(max_length=100, null=True)
    env = models.BooleanField(default=False)
    m12 = models.CharField(max_length=100, null=True)
    cuarto = models.CharField(max_length=100, null=True)
    zona = models.CharField(max_length=100, null=True)
    cant_artes = models.IntegerField(null=True)
    anio = models.IntegerField(null=True)


# Pareja.dbf
class Pareja(models.Model):
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    buque_pareja = models.ForeignKey(Buque, related_name='buquePareja', null=True)
    buque = models.ForeignKey(Buque, null=True)
    env = models.BooleanField()
    anio = models.IntegerField()


# Disposit.dbf
class Dispositivo(models.Model):
    nro_disp = models.CharField(max_length=100)
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    tamanio = models.FloatField(null=True)
    tipo_malla = models.CharField(max_length=100, null=True)
    luz_malla = models.FloatField(null=True)
    env = models.BooleanField()
    anio = models.IntegerField()

    def __unicode__(self):
        return self.nro_disp


# Motores.dbf
class Motor(models.Model):
    buque = models.ForeignKey(Buque, null=True)
    fch_pref = models.DateField(null=True)
    tipo = models.CharField(max_length=100, null=True)
    motor = models.CharField(max_length=100, null=True)
    nro_motor = models.CharField(max_length=100, null=True)
    modelo = models.CharField(max_length=100, null=True)
    pothp = models.FloatField(null=True)
    potkw = models.FloatField(null=True)
    cantmotaux = models.FloatField(null=True)
    potaux = models.FloatField(null=True)
    vigente = models.BooleanField()
    user_modi = models.CharField(max_length=100, null=True)
    fcha_modi = models.DateField(null=True)
    anio = models.IntegerField()

    def __unicode__(self):
        return self.buque + ' -Motor:' + self.motor


# Pote.dbf
class Potero(models.Model):
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    buque = models.ForeignKey(Buque, null=True)
    pot_fecha = models.DateTimeField(null=True)  # incluye campo hora de pote.dbf
    rectangulo = models.CharField(max_length=100, null=True)
    illex = models.FloatField(null=True)
    loligo = models.FloatField(null=True)
    martialla = models.FloatField(null=True)
    env = models.BooleanField()
    tot_esp = models.FloatField(null=True)
    porc_esp = models.FloatField(null=True)
    acta = models.IntegerField(null=True)
    tiene_acta = models.BooleanField()
    ajustado = models.FloatField(null=True)
    capt_pp = models.FloatField(null=True)
    fch_ajuste = models.DateField(null=True)
    lat_min = models.IntegerField(null=True)
    long_min = models.IntegerField(null=True)
    cuarto = models.CharField(max_length=100, null=True)
    anio = models.IntegerField()


# Espc.dbf
class Especie_captura(models.Model):
    jurisdiccion = models.ForeignKey(Jurisdiccion, null=True)
    parte = models.ForeignKey(Parte, null=True)
    cuadricula = models.ForeignKey(Rect_Parte, null=True)  # jurisdiccion+parte+rectangulo+area
    nro_espc = models.ForeignKey(Especie, null=True)
    fch_capt = models.DateField(null=True)  # son dia_capt y mes_capt en espc.dbf
    capt_kilo = models.FloatField(null=True)
    env = models.BooleanField(default=False)
    periodo = models.IntegerField(null=True)
    trb = models.IntegerField(null=True)
    metodo = models.CharField(max_length=100, null=True)
    rectangulo = models.CharField(max_length=100, null=True)
    area = models.CharField(max_length=100, null=True)
    diario = models.BooleanField(default=False)
    m12 = models.CharField(max_length=100, null=True)
    cuarto = models.CharField(max_length=100, null=True)
    zona = models.CharField(max_length=100, null=True)
    tot_esp = models.FloatField(null=True)
    porc_esp = models.FloatField(null=True)
    acta = models.IntegerField(null=True)
    tiene_acta = models.BooleanField(default=False)
    ajustado = models.FloatField(null=True)
    fch_ajuste = models.DateField(null=True)
    capt_pp = models.FloatField(null=True)
    capt_legal = models.FloatField(null=True)
    fch_legal = models.DateField(null=True)
    fch_app = models.DateField(null=True)
    anio = models.IntegerField(null=True)

    @staticmethod
    # determina la cantidad de viajes de un barco a una determinada especie en un anio dado
    def get_viajesXespecie(anio, buque, especie):
        total_viajes_especie = Especie_captura.objects.filter(anio=anio,
                                                              parte__buque=buque, nro_espc=especie). \
            distinct('parte').count()
        return total_viajes_especie

    @staticmethod
    # me da una lista de objs. buque que fueron a capturar la especie dada en un anio dado
    def get_buquesXespecie(anio, especie):
        buques = []
        lista_buques = Especie_captura.objects.filter(anio=anio, nro_espc=especie, diario=False).order_by(
            'parte__buque').distinct('parte__buque').values('parte__buque__matbuq')
        for b in lista_buques:
            try:
                buque = Buque.objects.get(anio=anio, matbuq=b['parte__buque__matbuq'])
                buques.append(buque)
            except Exception, e:
                print(e.message)
                pass
        return buques

    @staticmethod
    def get_buque_captura_total(anio, buque):
        capturaTotal = Especie_captura.objects.filter(anio=anio, parte__buque=buque). \
            aggregate(Sum('capt_kilo'))
        return capturaTotal

    @staticmethod
    def get_buque_captura_especie(anio, buque, especie):
        capturaTotal = Especie_captura.objects.filter(anio=anio,
                                                      parte__buque=buque, nro_espc=especie). \
            aggregate(Sum('capt_kilo'))
        return capturaTotal

    @staticmethod
    def get_partes_captura_especie(anio, especie):
        '''
        Pido las capturas de una especie agrupadas por parte (sin importar rect.)
        return: una lista de dict. con (kilos de esa especie, suma de lances y tiempos pesca, parte_id)
        '''
        return Especie_captura.objects.filter(anio=anio, nro_espc=especie).values('parte'). \
            annotate(Sum('capt_kilo'),Sum('cuadricula__lances'),Sum('cuadricula__tiem_pesca')). \
            order_by('parte')

    @staticmethod
    def arma_fch_capt(dia, mes, anio):
        import datetime
        fecha = ""
        if dia == '' or dia=='00':
            dia = 1
        if len(str(dia)) == 1:
            fecha = "0" + str(dia)
        else:
            fecha = str(dia)
        if mes == '' or mes =='00':
            mes=1
        if len(str(mes)) == 1:
            fecha += "0" + str(mes)
        else:
            fecha += str(mes)
        fecha += str(anio)
        return datetime.datetime.strptime(fecha, '%d%m%Y').date()


# Auditoria de errores en migracion partes de pesca
class AuditoriaImportacion(models.Model):
    lote = models.ForeignKey(Lote, null=True)
    modelo = models.CharField(max_length=50)
    error = models.CharField(max_length=200)
    linea = models.IntegerField()

    def __unicode__(self):
        return str(self.lote.id) + '/' + self.modelo + '/' + str(self.linea)


# - --------------------------
class ArchivoProducido(models.Model):
    nombre = models.FileField(upload_to='partes/producidos')
    descripcion = models.CharField(max_length=255)
    anio = models.IntegerField()
    tipo = models.CharField(max_length=20)
    fecha_creado = models.DateTimeField(blank=True, null=True, auto_now=True)

    def __unicode__(self):
        return str(self.nombre)
